source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.0.3"
gem "rails", "~> 7.0.2", ">= 7.0.2.2"
gem "pg", "~> 1.1"
gem "puma", "~> 5.0"
# gem "redis", "~> 4.0"
# gem "kredis"
gem "bcrypt", "~> 3.1.7"
gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]
gem "bootsnap", require: false
gem "active_model_serializers", "~> 0.10.13"
# gem "image_processing", "~> 1.2"
# gem "rack-cors"
gem "newrelic_rpm", "~> 8.5.0"

group :development, :test do
  gem "debug", platforms: %i[mri mingw x64_mingw]

  gem "factory_bot_rails", "~> 6.2"
  gem "faker", "~> 2.19"

  gem "rspec-core", "~> 3.10"
  gem "rspec-expectations", "~> 3.10", ">= 3.10.2"
  gem "rspec-mocks", "~> 3.10", ">= 3.10.2"
  gem "rspec-rails", "~> 5.0", ">= 5.0.2"

  gem "rubocop", "~> 1.23", require: false
  gem "rubocop-performance", "~> 1.12", require: false
  gem "rubocop-rails", "~> 2.12", ">= 2.12.4", require: false
  gem "rubocop-rspec", "~> 2.6", require: false
end

group :development do
  gem "spring"
  gem "annotate", "~> 3.1", ">= 3.1.1"
end

gem "simplecov", "~> 0.21.2", require: false, group: :test
