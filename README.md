<!-- TODO: Change logo image -->
<header align="center">
<!--
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img
    src="https://github.com/othneildrew/Best-README-Template/blob/master/images/logo.png?raw=true"
    alt="Logo"
    width="80"
    height="80">
  </a>
-->
  <center><h1>Fantasy Draft Board Authentication Service</h1></center>
</header>

## Table of Contents

- [Table of Contents](#table-of-contents)
- [About The Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
  - [Running](#running)
  - [Testing](#testing)
- [Contact](#contact)

## About The Project

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

Service to authenticate and authorize fantasy football members.

### Built With

- [Ruby](https://www.ruby-lang.org/en/)
- [Rails](https://rubyonrails.org/)
- [JWT](https://jwt.io/)

## Getting Started

### Prerequisites

- Ruby
- Rails
- Direnv

### Installation

1. Clone the repo

```sh
git clone <git@gitlab.com:grp-fantasy-draft-board/auth-api-v2.git>
```

2. Install project dependencies

```sh
bundle install
```

## Usage

### Running

```sh
bundle exec rails server
```

### Testing

```sh
bundle exec rspec
```

<!--
[ ] TODO: Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

[ ] TODO: _For more examples, please refer to the [Documentation](https://example.com)_
-->

## Contact

Noel Sagaille - [@mrbernnz](https://twitter.com/mrbernnz) - noel.sagaille@gmail.com

<!--
Project Link: [https://github.com/your_username/repo_name](https://github.com/your_username/repo_name)
-->
