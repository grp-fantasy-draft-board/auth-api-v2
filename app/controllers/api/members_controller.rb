class Api::MembersController < ApplicationController
  before_action :member, only: [:show, :update, :destroy]

  def show
    render json: @member,
           fields: { members: [:name, :email, :image_url, :owner_roles] },
           status: :ok
  end

  def update
    member_params
    @member.update(member_params)
    head :no_content
  end

  def destroy
    @member.destroy!
    head :no_content
  end

  private

  def member
    @member = Member.find(params[:id])
  end

  def member_params
    ActiveModelSerializers::Deserialization
      .jsonapi_parse(params,
                     only: [
                       :first_name, :last_name,
                       :email, :password,
                       :image_url
                     ])
    # params.permit(:first_name, :last_name, :email, :password, :image_url)
  end
end
