class ApplicationController < ActionController::API
  include Api::ErrorHandler

  before_action :authorize!

  def routing_error
    raise ActionController::RoutingError, params[:path]
  end

  private

  def authorize!
    raise ActionController::InvalidAuthenticityToken unless current_member
  end

  def current_member
    @current_member = access_token&.member
  end

  def access_token
    provided_token = request.authorization&.gsub(/\ABearer\s/, "")
    @access_token = AccessToken.find_by(token: provided_token)
  end
end
