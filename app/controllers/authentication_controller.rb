class AuthenticationController < ApplicationController
  skip_before_action :authorize!, only: :login

  def login
    authenticator = Authenticators::Member.new(**authentication_params)
    authenticator.perform

    render json: authenticator.access_token,
           status: :created
  end

  def logout
    current_member.access_token.destroy
    head :no_content
  end

  private

  def authentication_params
    (standard_auth_params || params.permit(:code)).to_h.symbolize_keys
  end

  def standard_auth_params
    ActiveModelSerializers::Deserialization
      .jsonapi_parse(params, only: [:email, :password])
    # params.dig(:data, :attributes)&.permit(:email, :password)
  end
end
