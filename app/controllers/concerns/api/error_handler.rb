module Api::ErrorHandler
  extend ActiveSupport::Concern

  ERRORS = {
    "AbstractController::ActionNotFound" => "Errors::NotFound",
    "ActionController::RoutingError" => "Errors::Routing",
    "ActionController::MethodNotAllowed" => "Errors::MethodNotAllowed",
    "ActionController::UnknownHttpMethod" => "Errors::MethodNotAllowed",
    "ActionController::BadRequest" => "Errors::BadRequest",
    "ActionController::ParameterMissing" => "Errors::BadRequest",
    "ActionController::NotImplemented" => "Errors::NotAcceptable",
    "ActionController::UnknownFormat" => "Errors::NotAcceptable",
    "ActionController::InvalidAuthenticityToken" => "Errors::Forbidden",
    "ActionDispatch::ParamsParser::ParseError" => "Errors::BadRequest",
    "ActiveRecord::RecordNotFound" => "Errors::NotFound",
    "ActiveRecord::RecordInvalid" => "Errors::Invalid",
    "ActiveRecord::RecordNotSaved" => "Errors::Invalid",
    "Authenticators::Standard::AuthenticationError" => "Errors::Unauthorized",
  }.freeze

  included do
    rescue_from(StandardError, with: ->(e) { handle_error(e) })
  end

  private_constant :ERRORS

  private

  def handle_error(error)
    mapped = map_error(error)
    mapped ||= Errors::ApiError.new
    render_error(mapped)
  end

  def map_error(error)
    error_klass = error.class.name
    return error if ERRORS.value?(error_klass)

    customer_error = ERRORS[error_klass]

    if error_klass == "ActiveRecord::RecordInvalid"
      customer_error&.constantize&.new error
    else
      customer_error&.constantize&.new
    end
  end

  def render_error(error)
    render json: ErrorSerializer.new(error), status: error.status
  end
end
