class RegistrationsController < ApplicationController
  skip_before_action :authorize!, only: :sign_up

  def sign_up
    # debugger
    member = Member.create!(registration_params)
    render json: member.create_access_token,
           status: :created
  end

  private

  def registration_params
    ActiveModelSerializers::Deserialization
      .jsonapi_parse(params, only: [
                       :first_name, :last_name,
                       :email, :password,
                       :image_url
                     ])
    # params.dig(:data, :attributes)&.permit(
    # :first_name, :last_name, :email, :password, :image_url
    # )
  end
end
