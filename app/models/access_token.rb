# == Schema Information
#
# Table name: access_tokens
#
#  id         :string           not null, primary key
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  member_id  :string           not null
#
# Indexes
#
#  index_access_tokens_on_member_id  (member_id)
#
# Foreign Keys
#
#  fk_rails_...  (member_id => members.id)
#
class AccessToken < ApplicationRecord
  belongs_to :member
  after_initialize :generate_token

  validates :token, presence: true, uniqueness: true

  private

  def generate_token
    loop do
      break if token.present? &&
        !AccessToken
          .where
          .not(id: id)
          .exists?(token: token)

      self.token = SecureRandom.hex(10)
    end
  end
end
