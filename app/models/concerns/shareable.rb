module Shareable
  extend ActiveSupport::Concern

  included do
    before_create :generate_id
  end

  def generate_id
    self.id = SecureRandom.alphanumeric.downcase[0..11] if id.blank?
  end
end
