# == Schema Information
#
# Table name: members
#
#  id                 :string(255)      not null, primary key
#  email              :string(255)      not null
#  encrypted_password :string(255)      not null
#  first_name         :string(255)      not null
#  image_url          :string(255)      not null
#  last_name          :string(255)      not null
#  created_at         :timestamptz      not null
#  updated_at         :timestamptz      not null
#
# Indexes
#
#  members_email_key  (email) UNIQUE
#
class Member < ApplicationRecord
  include BCrypt
  validates :first_name, :last_name, :email, :encrypted_password, :image_url, presence: true
  validates :email, uniqueness: true

  before_create :add_league_roles

  has_one :access_token, dependent: :destroy
  has_and_belongs_to_many :roles, join_table: :member_roles

  def password
    @password ||= Password.new(encrypted_password)
  end

  def password=(new_password)
    @password = Password.create(new_password)
    self.encrypted_password = @password
  end

  private

  def add_league_roles
    roles << league_owner_roles
  end

  def league_owner_roles
    regex = /owner|moderator/i
    Role.select { |role| role.designation.match?(regex) }
  end
end
