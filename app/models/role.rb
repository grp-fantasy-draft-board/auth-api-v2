# == Schema Information
#
# Table name: roles
#
#  id          :string(255)      not null, primary key
#  designation :enum             not null
#  created_at  :timestamptz      not null
#  updated_at  :timestamptz      not null
#
# Indexes
#
#  roles_designation  (designation)
#
class Role < ApplicationRecord
  has_and_belongs_to_many :members, join_table: :member_roles

  TYPES = { OWNER: "OWNER", MANAGER: "MANAGER", MODERATOR: "MODERATOR" }.freeze

  enum designation: TYPES
end
