# == Schema Information
#
# Table name: access_tokens
#
#  id         :string           not null, primary key
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  member_id  :string           not null
#
# Indexes
#
#  index_access_tokens_on_member_id  (member_id)
#
# Foreign Keys
#
#  fk_rails_...  (member_id => members.id)
#
class AccessTokenSerializer < ApplicationSerializer
  attributes :token, :member_id
end
