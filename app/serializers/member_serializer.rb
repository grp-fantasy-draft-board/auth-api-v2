# == Schema Information
#
# Table name: members
#
#  id                 :string(255)      not null, primary key
#  email              :string(255)      not null
#  encrypted_password :string(255)      not null
#  first_name         :string(255)      not null
#  image_url          :string(255)      not null
#  last_name          :string(255)      not null
#  created_at         :timestamptz      not null
#  updated_at         :timestamptz      not null
#
# Indexes
#
#  members_email_key  (email) UNIQUE
#
class MemberSerializer < ApplicationSerializer
  attributes :id, :first_name, :last_name,
             :name, :email, :password,
             :image_url, :owner_roles

  def name
    "#{object.first_name} #{object.last_name}"
  end

  def owner_roles
    object.roles.map(&:designation)
  end
end
