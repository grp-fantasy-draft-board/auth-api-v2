# == Schema Information
#
# Table name: roles
#
#  id          :string(255)      not null, primary key
#  designation :enum             not null
#  created_at  :timestamptz      not null
#  updated_at  :timestamptz      not null
#
# Indexes
#
#  roles_designation  (designation)
#
class RoleSerializer < ApplicationSerializer
  attributes :designation
end
