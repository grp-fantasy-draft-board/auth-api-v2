Rails.application.routes.draw do
  post "login", action: :login, controller: "authentication"
  delete "logout", action: :logout, controller: "authentication"
  post "signup", action: :sign_up, controller: "registrations"

  namespace :api do
    resources :members, only: [:show, :update, :destroy]
  end

  match "*path", to: "application#routing_error", via: :all
end
