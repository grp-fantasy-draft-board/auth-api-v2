class CreateMemberRolesJoinTable < ActiveRecord::Migration[7.0]
  def change
    create_join_table :members, :roles, table_name: :member_roles do |t|
      t.index :member_id
      t.index :role_id
    end

    reversible do |dir|
      dir.up do
        change_column :member_roles, :member_id, :string, limit: 255
        change_column :member_roles, :role_id, :string, limit: 255
      end

      dir.down do
        change_column :member_roles, :member_id, :integer, using: "member_id::integer"
        change_column :member_roles, :role_id, :integer, using: "role_id::integer"
      end
    end
  end
end
