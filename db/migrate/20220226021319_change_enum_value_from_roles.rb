class ChangeEnumValueFromRoles < ActiveRecord::Migration[7.0]
  def change
    reversible do |dir|
      dir.up do
        execute <<-SQL
          ALTER TYPE enum_roles_designation RENAME VALUE 'ADMIN' to 'OWNER';
        SQL
      end

      dir.down do
        execute <<-SQL
          ALTER TYPE enum_roles_designation RENAME VALUE 'OWNER' to 'ADMIN';
        SQL
      end
    end
  end
end
