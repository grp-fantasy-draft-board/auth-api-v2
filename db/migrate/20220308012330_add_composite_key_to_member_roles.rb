class AddCompositeKeyToMemberRoles < ActiveRecord::Migration[7.0]
  def change
    add_index :member_roles, [:member_id, :role_id], unique: true
  end
end
