class RenamePasswordFromMembers < ActiveRecord::Migration[7.0]
  def change
    rename_column :members, :password, :encrypted_password
  end
end
