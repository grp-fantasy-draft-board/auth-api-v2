# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 0) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  # Custom types defined in this database.
  # Note that some types may not work with other database engines. Be careful if changing database.
  create_enum "enum_roles_designation", %w[ADMIN MODERATOR MANAGER]

  create_table "SequelizeMeta", primary_key: "name", id: { type: :string, limit: 255 }, force: :cascade do |t|
  end

  create_table "members", id: { type: :string, limit: 255 }, force: :cascade do |t|
    t.string "first_name", limit: 255, null: false
    t.string "last_name", limit: 255, null: false
    t.string "email", limit: 255, null: false
    t.string "password", limit: 255, null: false
    t.string "image_url", limit: 255, null: false
    t.timestamptz "created_at", null: false
    t.timestamptz "updated_at", null: false
    t.index ["email"], name: "members_email_key", unique: true
  end

  create_table "password_resets", id: { type: :string, limit: 255 }, force: :cascade do |t|
    t.string "token", limit: 255, null: false
    t.bigint "expires_in", null: false
    t.string "member_id", limit: 255, null: false
    t.timestamptz "created_at", null: false
    t.timestamptz "updated_at", null: false
  end

  create_table "roles", id: { type: :string, limit: 255 }, force: :cascade do |t|
    t.enum "designation", null: false, enum_type: "enum_roles_designation"
    t.timestamptz "created_at", null: false
    t.timestamptz "updated_at", null: false
    t.index ["designation"], name: "roles_designation"
    t.check_constraint "designation = ANY (ARRAY['ADMIN'::enum_roles_designation, 'MODERATOR'::enum_roles_designation, 'MANAGER'::enum_roles_designation])",
                       name: "custom_check_role_constraint"
  end

  add_foreign_key "password_resets", "members", name: "password_resets_member_id_fkey"
end
