module Authenticators
  class Member
    attr_reader :authenticator, :access_token

    def initialize(email: nil, password: nil)
      @authenticator = Standard.new(email: email, password: password)
    end

    def perform
      authenticator.perform

      set_access_token
    end

    def member
      authenticator.member
    end

    private

    def set_access_token
      @access_token = member.access_token.presence || member.create_access_token
    end
  end
end
