module Authenticators
  class Standard < Authenticators::Member
    class AuthenticationError < StandardError; end

    attr_reader :member

    def initialize(email:, password:)
      @email = email
      @password = password
    end

    def perform
      raise AuthenticationError if email.blank? || password.blank?
      raise AuthenticationError unless ::Member.exists?(email: email)

      member = ::Member.find_by!(email: email)

      raise AuthenticationError unless member.password == password

      @member = member
    end

    private

    attr_reader :email, :password
  end
end
