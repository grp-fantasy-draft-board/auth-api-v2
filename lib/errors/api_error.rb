module Errors
  class ApiError < ::StandardError
    TITLE = "Something went wrong.".freeze
    DETAIL = "Encountered unexpected error and developers have been notified.".freeze

    def initialize(title: nil, detail: nil, status: nil, source: {})
      @title = title || TITLE
      @detail = detail || DETAIL
      @status = status || 500
      @source = source.deep_stringify_keys
      super
    end

    def to_h
      { status: status, title: title, detail: detail, source: source }
    end

    def serializable_hash
      to_h
    end

    def to_s
      to_h.to_s
    end

    attr_reader :title, :detail, :status, :source
  end
end
