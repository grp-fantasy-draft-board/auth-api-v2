module Errors
  class BadRequest < Errors::ApiError
    MESSAGE = "We could not complete your request.".freeze
    def initialize
      super(title: "Bad Request", status: 400, detail:  MESSAGE, source: {
        pointer: "/request",
      })
    end
  end
end
