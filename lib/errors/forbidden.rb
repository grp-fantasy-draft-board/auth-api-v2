module Errors
  class Forbidden < Errors::ApiError
    MESSAGE = "You have no right to access this resource.".freeze

    def initialize
      super(
        title: "Not authorized",
        status: 403,
        detail: MESSAGE,
        source: { pointer: "/headers/authorization" })
    end
  end
end
