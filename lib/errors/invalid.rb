module Errors
  class Invalid < Errors::ApiError
    TITLE = "Unprocessable Entity".freeze

    def initialize(error)
      @errors = error.record.errors.messages
      super(status: 422, title: TITLE)
    end

    def serializable_hash
      errors.reduce([]) do |err_array, (att, msg)|
        err_array << {
          status: status,
          title: title,
          detail: msg.first,
          source: { pointer: "/data/attributes/#{att}" },
        }
      end
    end

    private

    attr_reader :errors
  end
end
