module Errors
  class MethodNotAllowed < Errors::ApiError
    MESSAGE = "Method used not allowed".freeze

    def initialize
      super(title: "Method Not Allowed", status: 405, detail: MESSAGE, source: {
        pointer: "/request/method",
      })
    end
  end
end
