module Errors
  class NotAcceptable < Errors::ApiError
    MESSAGE = "Format unacceptable".freeze

    def initialize
      super(title: "Not Acceptable", status: 406, detail: MESSAGE, source: {
        pointer: "/request/headers/accept",
      })
    end
  end
end
