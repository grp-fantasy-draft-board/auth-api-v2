module Errors
  class NotFound < Errors::ApiError
    MESSAGE = "We could not find the member you were looking for.".freeze

    def initialize
      super(title: "Not Found", status: 404, detail: MESSAGE, source: { pointer: "/request/url/:id" })
    end
  end
end
