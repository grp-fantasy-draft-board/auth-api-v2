module Errors
  class NotImplemented < Errors::ApiError
    MESSAGE = "Cannot respond to the request.".freeze

    def initialize
      super(title: "Not Implemented", status: 501, detail: MESSAGE, source: {
        pointer: "/request",
      })
    end
  end
end
