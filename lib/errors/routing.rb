module Errors
  class Routing < Errors::ApiError
    MESSAGE = "No route matches".freeze

    def initialize
      super(title: "Not Found", status: 404, detail: MESSAGE, source: { pointer: "/request/url/" })
    end
  end
end
