module Errors
  class Unauthorized < Errors::ApiError
    DETAIL = "You must provide valid credentials in order to exchange for token.".freeze

    def initialize
      super(
        title: "Invalid email or password",
        status: 401,
        detail: DETAIL,
        source: { pointer: "/data/attributes/password" })
    end
  end
end
