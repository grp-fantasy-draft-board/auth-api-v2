# == Schema Information
#
# Table name: members
#
#  id                 :string(255)      not null, primary key
#  email              :string(255)      not null
#  encrypted_password :string(255)      not null
#  first_name         :string(255)      not null
#  image_url          :string(255)      not null
#  last_name          :string(255)      not null
#  created_at         :timestamptz      not null
#  updated_at         :timestamptz      not null
#
# Indexes
#
#  members_email_key  (email) UNIQUE
#
FactoryBot.define do
  factory :member do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    encrypted_password { Faker::Internet.password }
    image_url { Faker::Placeholdit.image }
  end
end
