require "rails_helper"

describe Authenticators::Member do
  email = Faker::Internet.email
  password = Faker::Internet.password
  let(:member) { create(:member, email: email, password: password) }

  shared_examples "authenticator" do
    it "creates and set user access token" do
      aggregate_failures do
        expect(authenticator.authenticator)
          .to receive(:perform)
          .and_return(true)

        expect(authenticator.authenticator)
          .to receive(:member)
          .at_least(:once)
          .and_return(member)
      end

      aggregate_failures do
        expect { authenticator.perform }.to change(AccessToken, :count).by(1)
        expect(authenticator.access_token).to be_present
      end
    end
  end

  context "when initialized member credentials" do
    let(:authenticator) do
      described_class.new(email: email, password: password)
    end
    let(:authenticator_class) { Authenticators::Standard }

    describe "#initialize" do
      it "initializes proper authenticator" do
        expect(authenticator_class)
          .to receive(:new)
          .with(email: email, password: password)
        authenticator
      end
    end

    it_behaves_like "authenticator"
  end
end
