require "rails_helper"

describe Authenticators::Standard do
  describe "#perform" do
    email =  Faker::Internet.email
    password = Faker::Internet.password
    let(:authenticator) do
      described_class.new(email: email, password: password)
    end

    shared_examples "invalid authentication" do
      before { member }

      it "raises an error" do
        aggregate_failures do
          expect { authenticator.perform }
            .to raise_error(Authenticators::Standard::AuthenticationError)
          expect(authenticator.member).to be_nil
        end
      end
    end

    context "when invalid email" do
      let(:member) do
        create(:member, email: "foo@bar.com", password: password)
      end

      it_behaves_like "invalid authentication"
    end

    context "when invalid password" do
      let(:member) do
        create(:member, email: email, password: "password")
      end

      it_behaves_like "invalid authentication"
    end

    context "when successfully authenticated" do
      let(:member) { create(:member, email: email, password: password) }

      before { member }

      it "sets the member found in db" do
        aggregate_failures do
          expect { authenticator.perform }.not_to change(Member, :count)
          expect(authenticator.member).to eq(member)
        end
      end
    end
  end
end
