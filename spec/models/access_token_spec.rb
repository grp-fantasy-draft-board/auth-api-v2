# == Schema Information
#
# Table name: access_tokens
#
#  id         :string           not null, primary key
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  member_id  :string           not null
#
# Indexes
#
#  index_access_tokens_on_member_id  (member_id)
#
# Foreign Keys
#
#  fk_rails_...  (member_id => members.id)
#
require "rails_helper"

RSpec.describe AccessToken, type: :model do
  describe "#validations" do
    it "has valid factory" do
      access_token = build(:access_token)
      expect(access_token).to be_valid
    end

    it "validates token" do
      access_token = create :access_token
      aggregate_failures do
        expect(build(:access_token, token: "")).to be_invalid
        expect(build(:access_token, token: access_token.token)).to be_invalid
      end
    end
  end

  describe "#new" do
    it "has a token present after initialize" do
      expect(described_class.new.token).to be_present
    end

    it "generates unique token" do
      member = create :member
      aggregate_failures do
        expect { member.create_access_token }
          .to change(described_class, :count).by(1)
        expect(member.build_access_token).to be_valid
      end
    end

    it "generates token once" do
      member = create :member
      access_token = member.create_access_token
      expect(access_token.token).to eq(access_token.reload.token)
    end
  end
end
