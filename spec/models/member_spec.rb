# == Schema Information
#
# Table name: members
#
#  id                 :string(255)      not null, primary key
#  email              :string(255)      not null
#  encrypted_password :string(255)      not null
#  first_name         :string(255)      not null
#  image_url          :string(255)      not null
#  last_name          :string(255)      not null
#  created_at         :timestamptz      not null
#  updated_at         :timestamptz      not null
#
# Indexes
#
#  members_email_key  (email) UNIQUE
#
require "rails_helper"

describe Member, type: :model do
  context "validations" do
    it "is valid record" do
      member = build :member
      expect(member).to be_valid
    end

    it "validates presence of attributes" do
      expect do
        create :member,
               first_name: nil, last_name: nil,
               email: nil, encrypted_password: nil, image_url: nil
      end.to raise_error ActiveRecord::RecordInvalid
    end

    it "validate uniqueness of email" do
      member = create :member

      expect { create :member, email: member.email }.to raise_error ActiveRecord::RecordInvalid

      second_member = create :member, email: Faker::Internet.email
      expect(second_member).to be_valid
    end
  end
end
