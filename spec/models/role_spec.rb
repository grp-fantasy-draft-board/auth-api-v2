# == Schema Information
#
# Table name: roles
#
#  id          :string(255)      not null, primary key
#  designation :enum             not null
#  created_at  :timestamptz      not null
#  updated_at  :timestamptz      not null
#
# Indexes
#
#  roles_designation  (designation)
#
require "rails_helper"

describe Role, type: :model do
  context "when created successfully" do
    let(:role) { create :role }

    it "is valid record" do
      expect(role).to be_valid
    end

    it "is saved record" do
      expect(role).to be_persisted
    end
  end

  context "when attribute validation fails" do
    it "raises an error for an invalid designation" do
      expect do
        role.designation = "SKIPPER"
      end.to raise_error NameError
    end
  end
end
