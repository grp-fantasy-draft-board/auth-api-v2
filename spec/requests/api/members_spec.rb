require "rails_helper"

describe "Api::Members", type: :request do
  let(:member) { create :member }
  let(:id) { Faker::Alphanumeric.alpha(number: 12) }
  let(:access_token) { member.create_access_token }
  let(:headers) { { "authorization" => "Bearer #{access_token.token}" } }

  describe "GET api/members#show" do
    context "when unauthorized" do
      before do
        get api_member_url(member.id)
      end

      it_behaves_like "forbidden_requests"
    end

    context "when authorized" do
      context "when member's id is valid" do
        before do
          get api_member_url(member.id), headers: headers
        end

        it "returns HTTP 200" do
          expect(response).to have_http_status(:ok)
        end

        it "returns a proper JSON" do
          expect(response.body).to look_like_json

          owner_roles = member.roles.pluck(:designation)

          aggregate_failures do
            expect(json_data[:id]).to eq(member.id)
            expect(json_data[:type]).to eq("members")
            expect(json_data_attributes).to eq(
              name: "#{member.first_name} #{member.last_name}",
              email: member.email,
              imageUrl: member.image_url,
              ownerRoles: owner_roles,
            )
          end
        end

        it "returns valid owner roles" do
          aggregate_failures do
            expect(json_data_attributes[:ownerRoles].count).to eq 2
            expect(json_data_attributes[:ownerRoles])
              .to contain_exactly("OWNER", "MODERATOR")
          end
        end
      end

      context "when member's id is invalid" do
        it "return HTTP 404" do
          get api_member_url(id), headers: headers
          expect(response).to have_http_status(:not_found)
        end
      end
    end
  end

  describe "PUT api/members#update" do
    context "when unauthorized" do
      before do
        params = { firstName: Faker::Name.first_name }
        put api_member_url(member.id), params: params
      end

      it_behaves_like "forbidden_requests"
    end

    context "when authorized" do
      context "when member's id is valid" do
        it "returns HTTP 204" do
          params = { firstName: Faker::Name.first_name }
          put api_member_url(member.id), params: params, headers: headers
          expect(response).to have_http_status(:no_content)
        end
      end

      context "when member's id is invalid" do
        it "return HTTP 404" do
          put api_member_url(id), headers: headers
          expect(response).to have_http_status(:not_found)
        end
      end

      context "when member params is invalid" do
        it "return HTTP error" do
          put api_member_url(id), headers: headers
          expect(response).to have_http_status(:not_found)
        end
      end
    end
  end

  describe "DELETE api/members#destroy" do
    context "when member's id is valid" do
      it "returns HTTP 204" do
        delete api_member_url(member.id), headers: headers
        expect(response).to have_http_status(:no_content)
      end
    end

    context "when member's id is invalid" do
      it "returns HTTP 404" do
        headers = { "AUTHORIZATION" => "Bearer #{access_token.token}" }
        delete api_member_url(id), headers: headers
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
