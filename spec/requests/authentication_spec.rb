require "rails_helper"

describe "Authentication", type: :request do
  describe "POST authentication#login" do
    let(:email) { Faker::Internet.email }
    let(:password) { Faker::Internet.password }

    let(:params) do
      {
        data: {
          attributes: {
            email: email, password: password
          },
        },
      }
    end

    context "when valid credentials are provided" do
      subject { post login_url, params: params }

      let(:member) { create(:member, email: email, password: password) }

      before { member }

      it "returns http 201" do
        subject
        expect(response).to have_http_status(:created)
      end

      it "returns a json response" do
        subject
        expect(response.body).to look_like_json
      end

      it "returns json containing token" do
        subject
        expect(json_data_attributes).to eq(
          {
            token: member.access_token.token,
            memberId: member.id,
          },
        )
      end
    end

    context "when no credentials provided" do
      before { post login_url }

      it_behaves_like "unauthorized_standard_requests"
    end

    context "when invalid email provided" do
      let(:member) { create(:member, email: "test@example.com", password: password) }

      before do
        post login_url, params: params
        member
      end

      it_behaves_like "unauthorized_standard_requests"
    end

    context "when invalid password provided" do
      let(:member) { create(:member, email: email, password: "password") }

      before do
        post login_url, params: params
        member
      end

      it_behaves_like "unauthorized_standard_requests"
    end
  end

  describe "DELETE authentication#logout" do
    context "when no authorization header provided" do
      before { delete logout_url }

      it_behaves_like "forbidden_requests"
    end

    context "when invalid authorization header provided" do
      before do
        delete logout_url, headers: { "Authorization" => "Invalid token" }
      end

      it_behaves_like "forbidden_requests"
    end

    context "when valid request" do
      let(:member) { create(:member) }
      let(:access_token) { member.create_access_token }

      before do
        delete logout_url,
               headers: { "Authorization" => "Bearer #{access_token.token}" }
      end

      it "returns 204 status code" do
        expect(response).to have_http_status(:no_content)
      end

      it "remove access token from" do
        expect(AccessToken.exists?(member_id: member.id)).to be_falsey
      end
    end
  end
end
