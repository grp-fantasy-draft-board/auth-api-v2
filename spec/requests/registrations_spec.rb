require "rails_helper"

RSpec.describe "Registrations", type: :request do
  describe "POST /sign_up" do
    context "when invalid data provided" do
      let(:params) do
        {
          data: {
            attributes: {
              first_name: nil,
              last_name: nil,
              email: nil,
              password: nil,
              image_url: nil,
            },
          },
        }
      end

      before { post signup_url, params: params }

      it "return unprocessible entity status code" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "does not create a members" do
        email = params[:data][:attributes][:email]
        expect(Member.exists?(email: email)).to be_falsey
      end

      it "return error messages in response body" do
        expect(json[:errors]).to include(
          {
            status: 422, title: "Unprocessable Entity",
            detail: "can't be blank",
            source: { pointer: "/data/attributes/first_name" }
          },
          {
            status: 422, title: "Unprocessable Entity",
            detail: "can't be blank",
            source: { pointer: "/data/attributes/last_name" }
          },
          {
            status: 422, title: "Unprocessable Entity",
            detail: "can't be blank",
            source: { pointer: "/data/attributes/email" }
          },
          {
            status: 422, title: "Unprocessable Entity",
            detail: "can't be blank",
            source: { pointer: "/data/attributes/image_url" }
          },
        )
      end
    end

    context "when valid data provided" do
      let(:params) do
        {
          data: {
            attributes: {
              first_name: Faker::Name.first_name,
              last_name: Faker::Name.last_name,
              email: Faker::Internet.email,
              password: Faker::Internet.password,
              image_url: Faker::Placeholdit.image,
            },
          },
        }
      end

      before do
        post signup_url, params: params
      end

      it "returns http success" do
        expect(response).to have_http_status(:created)
      end

      it "create a member" do
        email = params[:data][:attributes][:email]
        expect(Member.exists?(email: email)).to be_truthy
      end

      it "returns correct JSON" do
        expect(json_data_attributes).to include(
          :token, :memberId
        )
      end
    end
  end
end
