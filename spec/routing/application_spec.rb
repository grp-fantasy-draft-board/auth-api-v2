require "rails_helper"

describe "ApplicationController", type: :routing do
  describe "routing" do
    it "routes to non existing path" do
      expect(get: "/members").to route_to(
        controller: "application",
        action: "routing_error",
        path: "members",
      )
    end
  end
end
