require "rails_helper"

describe "AuthenticationController", type: :routing do
  describe "routing" do
    it "routes to #create" do
      expect(post: "/login").to route_to(
        controller: "authentication",
        action: "login",
      )
    end

    it "routes to #destroy" do
      expect(delete("/logout")).to route_to(
        controller: "authentication",
        action: "logout",
      )
    end
  end
end
