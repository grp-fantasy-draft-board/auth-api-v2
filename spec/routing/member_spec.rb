require "rails_helper"

describe "Api::MembersController", type: :routing do
  describe "routing" do
    it "routes to #show" do
      expect(get: "/api/members/1").to route_to(
        controller: "api/members",
        action: "show",
        id: "1",
      )
    end

    it "routes to #update for put" do
      expect(put: "/api/members/1").to route_to(
        controller: "api/members",
        action: "update",
        id: "1",
      )
    end

    it "routes to #update for patch" do
      expect(patch: "/api/members/1").to route_to(
        controller: "api/members",
        action: "update",
        id: "1",
      )
    end

    it "routes to #destroy" do
      expect(delete: "/api/members/1").to route_to(
        controller: "api/members",
        action: "destroy",
        id: "1",
      )
    end
  end
end
