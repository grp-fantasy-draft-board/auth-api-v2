require "rails_helper"

describe "Registraion Routes" do
  it "routes to registrations#signup" do
    expect(post: "/signup").to route_to(
      controller: "registrations",
      action: "sign_up",
    )
  end
end
