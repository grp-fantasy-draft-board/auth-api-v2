module ApiHelpers
  def json
    JSON.parse(response.body).deep_symbolize_keys
  end

  def json_data
    json[:data]
  end

  def json_data_attributes
    json_data[:attributes]
  end
end
